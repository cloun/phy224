import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def model_func(I, gamma):
    B = (0.8)**(1.5)* 4*np.pi*(1e-7) * n * I /R

    return (0.5/np.pi)*gamma * B

# returns chi squared to determine goodness of fit
def chiValue(yValues, yErrors, predictedValues, numParams):
    insideSum = (yValues - predictedValues)/yErrors
    insideSum = insideSum**2

    return 1/(len(yValues)-numParams)*np.sum(insideSum)

# constants
# number of turns in coil
n = 320
# radius of coils in meters
R = 0.075
# ideal gyromagnetic ratio
gammaIdeal = (1.602e-19)/(2*9.1e-31)

# getting the data
currents, freq = np.loadtxt('data.txt', skiprows = 1, unpack = True)

# determining uncertainties
# for current it's just the specifications of the multimeter
# for values over 400mA: 2.5% rdg + 10 last digit (0.01)
# for values uncer 400mA: 2.0% rdg + 10 last digit (0.01)
currentUnc = np.zeros(len(currents))
for i in range(len(currents)):
    if currents[i] <= 0.4:
        currentUnc[i] = 0.02*currents[i] + 0.01
    else:
        currentUnc[i] = 0.025*currents[i] + 0.01

# the current is not the peak current, it's the RMS, and so to convert to the peak current I multiple by srqt(2)
currents = currents * np.sqrt(2)
currentUnc = currentUnc * np.sqrt(2)

# for the frequency the manual says uncertainty is just LSD
# however since the resonant frequency wasn't perfectly clear, I'm adding a width
# of 0.5 kHz to the peak frequency, so just let uncertainty be 0.4 kHz
freqUnc = np.zeros(len(freq)) + 0.4

# curve fitting to eq 1
popt, pcov = curve_fit(model_func, currents, freq*1000000, p0 = [gammaIdeal], sigma = freqUnc*1000000, absolute_sigma= True)
fitUnc = np.sqrt(np.diag(pcov))[0]
yPredict = model_func(currents, popt[0])/1000000


# printing out curve fit values
print('Curve Fit Values:')
print('Gyromagnetic Ratio: {0:.4} +/- {1:.1}'.format(popt[0], fitUnc))
print('Free electron GMR: {0:.5}'.format(gammaIdeal))
print('Lande g factor: {0:.3f} +/- {1:.1}'.format(popt[0]/gammaIdeal, fitUnc/gammaIdeal))
print('Chi Value: {0:.3}'.format(chiValue(freq, freqUnc, yPredict, 1)))


plt.figure(figsize = (12, 7))
plt.title('Resonant Frequency of Photons against Current in Helmholtz Coils', fontsize= 20)
plt.ylabel('Frequency (MHz)', fontsize= 16)
plt.xlabel('Current (A)', fontsize= 16)
plt.errorbar(currents, freq, yerr= freqUnc, xerr = currentUnc, ls = 'none', label = 'Measured Data')
plt.plot(currents, yPredict, label = 'Curve fit to Eq 8')
plt.legend(loc = 'upper left')

plt.show()