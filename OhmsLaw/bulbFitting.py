from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt

def linearModelFunc(VLog, pow, coeffLog):
    return pow*VLog + coeffLog

def powerModelFunc(V, pow, coeff):
    return coeff*(V**pow)

# function to get reduced Chi squared values
def getChi(yValues, yErrors, predictedValues, numParams):
    insideSum = (yValues - predictedValues)/yErrors
    insideSum = insideSum**2

    return 1/(len(yValues)-numParams)*np.sum(insideSum)

voltage, current = np.loadtxt('LampMeasurements.txt', skiprows = 1, unpack= True, usecols= [0, 1])

# trimming the measurements in which the lamp was not glowing
# for these measurements the lamp was basically acting like a resistor, so it would 
# have shown a linear relation rather than following a power law.
voltage = voltage[8:]
current = current[8:]

# determining the uncertainties in the y
# the current ranged from 0.007 A to 0.2262 A and so the uncertainty given is either
# +/- 0.75% of the value, or +/- 0.0001 (from fluctuations in the last digit)
yErrors = []
# cycles through the y values and assigns each an error
for value in current:
    yErrors.append(max(0.0001, 0.0075*value))

# curve fitting with the different functions
poptLin, pcovLin = curve_fit(linearModelFunc, np.log(voltage), np.log(current), sigma= yErrors, p0=[1, 0.5882], absolute_sigma= True)
poptPow, pcovPow = curve_fit(powerModelFunc, voltage, current, sigma = yErrors, p0=[1, 0.5882], absolute_sigma= True)

# determining errors in the fitted parameters
pstdLin = np.sqrt(np.diag(pcovLin))
pstdPow = np.sqrt(np.diag(pcovPow))

# y values for the curve fits
yLin = powerModelFunc(voltage, poptLin[0], np.exp(poptLin[1]))
yPow = powerModelFunc(voltage, poptPow[0], poptPow[1])

# determing reduced chi values
chiLin = getChi(current, yErrors, yLin, 2)
chiPow = getChi(current, yErrors, yPow, 2)

# printing out relevant data
print("Linear Curve Fit:")
print('power = {0:.4f} +/- {1:.1}'.format(poptLin[0], pstdLin[0]))
print('coefficient = {0:.3f} +/- {1:.1}'.format(np.exp(poptLin[1]), np.exp(pstdLin[1])))
print('reduced Chi squared = {0:.2f}'.format(chiLin))
print()
print('Non-Linear Curve Fit:')
print('power = {0:.4f} +/- {1:.1}'.format(poptPow[0], pstdPow[0]))
print('coefficient = {0:.3f} +/- {1:.1}'.format(poptPow[1], pstdPow[1]))
print('reduced Chi squared  = {0:.2f}'.format(chiPow))


# plotting
plt.figure(figsize = (10, 6))

# changing scale to log
#plt.yscale('log')
#plt.xscale('log')

plt.ylabel('Current (A)')
plt.xlabel('Voltage (V)')
plt.title('Current Against Voltage of a Lit Bulb')
plt.errorbar(voltage, current, yErrors, elinewidth= 2., ls = 'none', label= 'Measured Data')
plt.plot(voltage, yLin, color = 'red', label= r'Linear Curve Fit')
plt.plot(voltage, yPow, color = 'green', label = r'Non-Linear Curve Fit')
plt.legend(loc = 'upper left')


plt.show()