import scipy.optimize as scp
import numpy as np
import matplotlib.pyplot as plt

# form of the function we're trying to fit
def model_function(V, R, yshift):
    return V/R + yshift

# form of the function without the yshift
def model_function_forced(V, R):
    return V/R

# function to get reduced chi squared
def getChi(yValues, yErrors, predictedValues, numParams):
    insideSum = (yValues - predictedValues)/yErrors
    insideSum = insideSum**2

    return 1/(len(yValues)-numParams)*np.sum(insideSum)

potentiometerFile = 'PotentiometerMeasurements.txt'
resistorFile = 'ResistorMeasurements.txt'

# current text file being curve fitted
textFile = potentiometerFile

# retrieving voltage and current from the text file (ignores the first row because it contains the column titles)
voltage, current = np.loadtxt(textFile, unpack = True, skiprows=1)

# determining the uncertainties in the y
# the current ranged from 0.007 A to 0.2262 A and so the uncertainty given is either
# +/- 0.75% of the value, or +/- 0.0001 (from fluctuations in the last digit)
yErrors = np.array([])
# cycles through the y values and assigns each an error
for value in current:
    yErrors = np.append(yErrors, max(0.0001, 0.0075*value))

print(0.0025*voltage)
print(yErrors)

# fitting to the curve
popt, pcov = scp.curve_fit(model_function, voltage, current, p0 = [99.17, 0.], sigma = yErrors, absolute_sigma = True)
pstd = np.sqrt(np.diag(pcov))


# fitting to the curve with forced yshift = 0
poptF, pcovF = scp.curve_fit(model_function_forced, voltage, current, p0 = [99.17], sigma = yErrors, absolute_sigma= True)
pstdF = np.sqrt(np.diag(pcovF))


# determining chi^2
chiSquaredyshift = getChi(current, yErrors, model_function(voltage, popt[0], popt[1]), 2)
chiSquared = getChi(current, yErrors, model_function_forced(voltage, poptF[0]), 1)

# printing out important info for each curve fit
print('Function with yshift:')
print('Resistance = {0:.2f} +/- {1:.1} Ohms'.format(popt[0], pstd[0]))
print('yshift = {0:.5f} +/- {1:.1} A'.format(popt[1], pstd[1]))
print('Reduced Chi squared = {0:.3f}'.format(chiSquaredyshift))
print()
print('Function without yshift:')
print('Resistance = {0:.2f} +/- ${1:.1} Ohms'.format(poptF[0], pstdF[0]))
print('Reduced Chi squared = {0:.3f}'.format(chiSquared))


# plotting
plt.figure(figsize = (10, 6))
plt.title(r'Current Plotted Against Voltage of a 1748.7 $\Omega$ Resistor')
plt.errorbar(voltage, current, yerr = yErrors, elinewidth = 2., ls = 'none', color='green', label = 'Measured Current')
plt.plot(voltage, model_function(voltage, popt[0], popt[1]), color = 'red', label = r'Fitted Function (R $\approx$ {0:.2f} $\Omega$, yshift $\approx$ {1:.2})'.format(popt[0], popt[1]))
# only used labelled resistance for the fixed resistor, the potentiometer knob didn't really have clear labelling on it
#plt.plot(voltage, model_function(voltage, 100., 0.), color = 'blue', label = r'Labelled Resistance(R = 100 $\Omega$)')
plt.plot(voltage, model_function_forced(voltage, poptF[0]), color = 'purple', label = r'Fitted Function, forced 0 yshift (R $\approx$ {0:.2f} $\Omega$)'.format(poptF[0]))
plt.plot(voltage, model_function(voltage, 1748.7, 0.), color = 'gray', label = r'Measured Resistance(R $\approx$ 1748.7 $\Omega$)')
plt.xlabel('Voltage (V)')
plt.ylabel('Current (A)')
plt.legend(loc = 'upper left')

plt.show()