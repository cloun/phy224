import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# model function for the fit
def model_func(angleIncidence, refractiveIndex, maxInt):
    angleRefraction = np.arcsin((1/refractiveIndex)*np.sin(angleIncidence))

    numerator = np.cos(angleRefraction) - refractiveIndex*np.cos(angleIncidence)
    denominator = np.cos(angleRefraction) + refractiveIndex*np.cos(angleIncidence)

    return maxInt*(numerator/denominator)**2

# function to get reduced Chi squared values
def getChi(yValues, yErrors, predictedValues, numParams):
    insideSum = (yValues - predictedValues)/yErrors
    insideSum = insideSum**2

    return 1/(len(yValues)-numParams)*np.sum(insideSum)

# text file we're reading the data from
dataFile = 'Data/brewster(start40)(10gain)4 CLEANED.txt'

angle, intensity = np.loadtxt(dataFile, skiprows = 2, unpack = True)

# converting angle to angle of incidence in degrees
angle = (180 - (angle-140))/2
# converting to radians
angle = angle*np.pi / 180
# converting the intensity to a percentage
intensity = intensity/max(intensity)

# uncertainties, same as in the malus law
# taking the uncertainty in the radians to be 0.05 since the device specifications says it is accurate to 1/4 degrees (0.04 rad)
RadUnc = np.zeros(len(angle)) + 0.04
# on observing the intensity for some time, the overall fluctuations were +/- 0.01 V
IntUnc = np.zeros(len(intensity)) + 0.025

# fitting to the equation
popt, pcov = curve_fit(model_func, angle, intensity, sigma = IntUnc, absolute_sigma= True, p0 = [1.495, 4.5])
cfUnc = np.sqrt(np.diag(pcov))
# getting y values
predictY = model_func(angle, popt[0], popt[1])

# getting the brewster's angle
brewster = np.arctan(popt[0])
brewsterUnc = max(abs(brewster - np.arctan(popt[0] + cfUnc[0])), abs(brewster - np.arctan(popt[0] - cfUnc[0])))

# getting the chi square reduced
chiSquare = getChi(intensity, IntUnc, predictY, 2)

# printing out curve fit values
print('Curve Fit Values:')
print('Refractive index: {0:.4f} +/- {1:.1}'.format(popt[0], cfUnc[0]))
print('Brewsters Angle: {0:.4f} +/- {1:.1} Rad'.format(brewster, brewsterUnc))
print('Max Intensity (Not particularly relevant): {0:.3f} +/- {1:.1} V'.format(popt[1], cfUnc[1]))
print('Chi Square: {0:.3}'.format(chiSquare))

plt.figure(figsize = (10, 6))
plt.title('Intensity of Light with Varying Angle of Incidence on Acrylic', fontsize = 15)
plt.ylabel('Relative Intensity (0 to 1)', fontsize = 12)
plt.xlabel('Angle of Incidence (Rad)', fontsize = 12)
plt.errorbar(angle, intensity, yerr= IntUnc, xerr = RadUnc, ls='none', label = 'Measured Data')
plt.plot(angle, predictY, label = 'Curve Fit to Equation 11')

plt.show()