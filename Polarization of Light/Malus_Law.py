import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# model function for malus' law
def malus_model(theta, Int0, b):
    return Int0*(np.cos(theta+b))**2

# model function for triple polarizer
def model_triplePolar(theta, Int0, b):
    return (Int0/4)*(np.sin(2*(theta +b)))**2

# function to get reduced Chi squared values
def getChi(yValues, yErrors, predictedValues, numParams):
    insideSum = (yValues - predictedValues)/yErrors
    insideSum = insideSum**2

    return 1/(len(yValues)-numParams)*np.sum(insideSum)

### DOUBLE POLARIZER
Rad, Int = np.loadtxt('Data/Malus_Law1Polar.txt', skiprows = 2, unpack= True)
# determining uncertainties
# taking the uncertainty in the radians to be 0.05 since the device specifications says it is accurate to 1/4 degrees (0.04 rad)
RadUnc = np.zeros(len(Rad)) + 0.04
# on observing the intensity for some time, the overall fluctuations were +/- 0.01 V
IntUnc = np.zeros(len(Int)) + 0.025

poptMalus, pcovMalus = curve_fit(malus_model, Rad, Int, p0 = [3.4, 0], sigma = IntUnc, absolute_sigma = True)
uncMalus = np.sqrt(np.diag(pcovMalus))
yPredictDouble = malus_model(Rad, poptMalus[0], poptMalus[1])

doubleChi = getChi(Int, IntUnc, yPredictDouble, 2)

# printing out curve fit data
print("Double Polarizer Malus\' Law Curve Fit Data:")
print('Max Intensity: {0:.4f} +/- {1:.2} V'.format(poptMalus[0], uncMalus[0]))
print('Radian Offset: {0:.4f} +/- {1:.1}'.format(poptMalus[1], uncMalus[1]))
print('Reduced Chi Squared: {0:.3}'.format(doubleChi))
print()

### TRIPLE POLARIZER
RadTriple, IntTriple = np.loadtxt('Data/Malus_Law3Polar.txt', skiprows = 2, unpack = True)
# Same uncertainties
RadUncTriple = np.zeros(len(RadTriple)) + 0.04
IntUncTriple = np.zeros(len(IntTriple)) + 0.025

poptTriple, pcovTriple = curve_fit(model_triplePolar, RadTriple, IntTriple, p0 = [3.4, -np.pi/2], sigma = IntUncTriple, absolute_sigma = True)
uncTriple = np.sqrt(np.diag(pcovTriple))
yPredictTriple = model_triplePolar(RadTriple, poptTriple[0], poptTriple[1])

tripleChi = getChi(IntTriple, IntUncTriple, yPredictTriple, 2)

# printing out curve fit values
print("Triple Polarizer Malus\' Law Curve Fit Data:")
print("Max Intensity: {0:.4f} +/- {1:.1} V".format(poptTriple[0], uncTriple[0]))
print("Radian Offset: {0:.4f} +/- {1:.2}".format(poptTriple[1], uncTriple[1]))
print('Reduced Chi Squared: {0:.3}'.format(tripleChi))
print()

# PLOTTING
plt.figure(figsize= (10, 6))
plt.title('Double Polarizer Intensity Versus Relative Polarizer Angle', fontsize = 15)
plt.ylabel('Intensity (V)', fontsize = 12)
plt.xlabel('Angle (rad)', fontsize = 12)
plt.errorbar(Rad, Int, yerr = IntUnc, xerr = RadUnc, ls = 'none', label = 'Measured Data')
plt.plot(Rad, yPredictDouble, label = 'Curve Fit to Equation 9')
plt.legend(loc = 'upper right')

plt.figure(figsize = (10,6))
plt.title('Triple Polarizer Intensity Versus Middle Polarizer Angle', fontsize = 15)
plt.ylabel('Intensity (V)', fontsize = 12)
plt.xlabel('Angle (rad)', fontsize = 12)
plt.errorbar(RadTriple, IntTriple, yerr = IntUncTriple, xerr = RadUncTriple, ls = 'none', label = 'Measured Data')
plt.plot(RadTriple, yPredictTriple, label = 'Curve Fit to Equation 10')
plt.legend(loc = 'upper right')

plt.show()