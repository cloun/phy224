import numpy as np
import matplotlib.pyplot as plt

# text files we're reading the data from
dataFile = 'Data/brewster(start40)(10gain)4.txt'
dataFile2 = 'Data/brewster(start40)(10gain)4 CLEANED.txt'

angle, intensity = np.loadtxt(dataFile, skiprows = 2, unpack = True)

# converting angle to angle of incidence in degrees
angle = (180 - (angle-140))/2
# converting to radians
angle = angle*np.pi / 180
# converting the intensity to a percentage
intensity = intensity/max(intensity)

# uncertainties, same as in the malus law
# taking the uncertainty in the radians to be 0.05 since the device specifications says it is accurate to 1/4 degrees (0.04 rad)
RadUnc = np.zeros(len(angle)) + 0.04
# on observing the intensity for some time, the overall fluctuations were +/- 0.01 V
IntUnc = np.zeros(len(intensity)) + 0.025

angleLine = 0.979
angleLineY = np.linspace(0, 1, 20)
zeroX = np.zeros(len(angleLineY))

Rad2, Int2 = np.loadtxt(dataFile2, skiprows = 2, unpack = True)

# converting angle to angle of incidence
Rad2 = (180 - (Rad2 - 140))/2
# converting to radians
Rad2 = Rad2*np.pi/180
# converting the intensity to a percentage
Int2 = Int2/max(Int2)

RadUnc2 = np.zeros(len(Rad2)) + 0.04
IntUnc2 = np.zeros(len(Int2)) + 0.025

plt.figure(figsize = (12, 8))
plt.subplot(1, 2, 1)
plt.title('Measured Data Before Cleaning', fontsize = 15)
plt.ylabel('Relative Intensity (0 to 1)', fontsize = 12)
plt.xlabel('Angle of Incidence (Rad)', fontsize = 12)
plt.errorbar(angle, intensity, yerr = IntUnc, xerr = RadUnc, ls = 'none')
plt.subplot(1, 2, 2)
plt.title('Measured Data After Cleaning', fontsize = 15)
plt.ylabel('Relative Intensity (0 to 1)', fontsize = 12)
plt.xlabel('Angle of Incidence (Rad)', fontsize = 12)
plt.errorbar(Rad2, Int2, yerr = IntUnc2, xerr = RadUnc2, ls= 'none')

plt.tight_layout()
plt.show()