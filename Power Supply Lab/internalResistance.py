import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

# model function for curve_fit
def model_func(I, R, Vinf):
    return Vinf - I*R

# returns the uncertainty from multiplication 
def multiplyUncertainty(values, uncertainties, finalValue):
    values = np.array(values)
    uncertainties = np.array(uncertainties)

    rootValue = np.sqrt(np.sum((uncertainties/values)**2))
    return abs(finalValue)*rootValue

# returns uncertainty from addition
def addUncertainty(uncertainties):
    uncertainties = np.array(uncertainties)
    return np.sqrt(np.sum(uncertainties**2))

# returns chi squared to determine goodness of fit
def chiValue(yValues, yErrors, predictedValues, numParams):
    insideSum = (yValues - predictedValues)/yErrors
    insideSum = insideSum**2

    return 1/(len(yValues)-numParams)*np.sum(insideSum)

# BATTERY
batteryCurrent, batteryVoltage = np.loadtxt('BatteryMeasurements.txt', unpack = True, skiprows = 1)

# determining uncertainty
# according to the manual uncertainty in current is 0.05% of reading + 2*count of least significant digit
batteryVoltUnc = batteryVoltage*0.0005 + 0.002

popt, pcov = curve_fit(model_func, batteryCurrent, batteryVoltage, p0= [0, 6.3], sigma = batteryVoltUnc, absolute_sigma = True)
batUncertainty = np.sqrt(np.diag(pcov))

# getting voltage values according to the curve fit
batteryVoltageCF = model_func(batteryCurrent, popt[0], popt[1])
# determining chi squared value
batChiSquare = chiValue(batteryVoltage, batteryVoltUnc, batteryVoltageCF, 2)

# printing out the curve fit values
print('Battery Values:')
print('Internal Resistance: {0:.2f} +/- {1:.1} Ohm'.format(popt[0], batUncertainty[0]))
print('Output Voltage: {0:.3f} +/- {1:.1} V'.format(popt[1], batUncertainty[1]))
print('Chi Squared: {0:.2}'.format(batChiSquare))
print()



# plotting battery data
plt.figure(figsize = (6,4))
plt.title('Current plotted against Voltage of a 6.5V Battery')
plt.xlabel('Current (A)')
plt.ylabel('Voltage (V)')
plt.errorbar(batteryCurrent, batteryVoltage, yerr = batteryVoltUnc, ls= 'none', label  = 'Measured Data')
plt.plot(batteryCurrent, batteryVoltageCF, label = r'$V = V_{\infty}-IR$ fitted function')
plt.legend(loc = 'upper right')

# POWER SUPPLY
PSCurrent, PSVoltage, PSSetting = np.loadtxt('PSMeasurements.txt', unpack = True, skiprows = 1)

# separating current values and voltage values based on the PS voltage setting (there were 4 voltage settings with 3 measurements each)
PSCurrentSep = np.reshape(PSCurrent, (4,3))
PSVoltageSep = np.reshape(PSVoltage, (4,3))

# stores all the internal resistance values and their uncertainties to average and find out the uncertainty value
psIntResist = []
psIntResistUnc =  []

# looping through each subarray and doing the curve_fit and plotting
plt.figure(figsize = (12,9))
for i in range(4):
    currents = PSCurrentSep[i]
    voltages = PSVoltageSep[i]

    # determining uncertainty the same way as earlier since the same multimeter setting was used
    uncertainties = (voltages*0.0005+0.002)

    # curve fitting and getting the uncertainties
    PSopt, PScov = curve_fit(model_func, currents, voltages, p0=[0, PSSetting[i*3]], sigma = uncertainties, absolute_sigma= True)
    CFUnc = np.sqrt(np.diag(PScov))

    # getting corresponding voltage values according to curve fit
    y = model_func(currents, PSopt[0], PSopt[1])
    # getting chi squared
    chiSquare = chiValue(voltages, uncertainties, y, 2)

    # printing out the values
    print('Power Supply Values for the {0:.1f} V Setting'.format(PSSetting[i*3]))
    print('Internal Resistance: {0:.3} +/- {1:.1} Ohms'.format(PSopt[0], CFUnc[0]))
    print('Output Voltage: {0:.3} +/- {1:.1}'.format(PSopt[1], CFUnc[1]))
    print('x-intercept: {0:.3} +/- {1:.1} A'.format(PSopt[1]/PSopt[0], multiplyUncertainty(PSopt, CFUnc, PSopt[1]/PSopt[0])))
    print('Chi Squared: {0:.2}'.format(chiSquare))
    print()

    # adds the values to the arrays to average them later
    psIntResist.append(PSopt[0])
    psIntResistUnc.append(CFUnc[0])

    # plotting each set in subplots
    plt.subplot(4, 1, i+1)
    plt.title(r'Power Supply Set to {0:.1f} V'.format(PSSetting[i*3]))
    plt.xlabel('Current (A)')
    plt.ylabel('Voltage (V)')
    plt.errorbar(currents, voltages, yerr = uncertainties, ls = 'none', label = 'Measured Data')
    plt.plot(currents, y, label = r'$V = V_\infty - IR$ fitted function')
    plt.legend(loc = 'upper right')


# determines average internal resistance and its uncertainty from the curve fitted values
print('Average Internal Resistance of Power Supply:  {0:.3f}  +\-  {1:.1}'.format(np.average(np.array(psIntResist)), addUncertainty(psIntResistUnc)/4))

plt.tight_layout()
plt.show()