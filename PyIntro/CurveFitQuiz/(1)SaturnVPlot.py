import matplotlib.pyplot as plt
import numpy as np

times, pos, uncertainty = np.loadtxt(fname = 'rocket.csv', delimiter = ',', unpack = True, skiprows = 1)


plt.xlabel(r'Time (hours)')
plt.ylabel(r'Position (km)')
plt.title('Position of Saturn V Rocket with uncertainties')
plt.errorbar(times, pos, yerr = uncertainty, ls = 'none', elinewidth = 2.)
plt.savefig('SaturnVPosvsTime.pdf')
plt.show()