import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stt

times, pos, uncertainty = np.loadtxt(fname = 'rocket.csv', delimiter = ',', unpack = True, skiprows = 1)

# I first tried to use the differences in position to determine the average speed and std dev
# but that gave an absurd std.dev. so I switched to this. This seems like a weird way to go about it
# but the average speed given is reasonably close to the previous average speed, and the std. dev. is much more reasonable
# removed the first value from each array since it is 0/0
speeds = pos[1:]/times[1:]

print("Mean of the Velocities = {0}\nStd. Dev of the Velocities = {1}".format(np.mean(speeds), stt.tstd(speeds)))