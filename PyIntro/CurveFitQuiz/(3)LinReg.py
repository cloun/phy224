import numpy as np

times, pos, uncertainty = np.loadtxt(fname = 'rocket.csv', delimiter = ',', unpack = True, skiprows = 1)

averageDist = np.mean(pos)
averageTime = np.mean(times)

speedEst = np.sum((times - averageTime)*(pos - averageDist))/(np.sum((times - averageTime)**2))

distEst = averageDist - (speedEst*averageTime)

print("Best Estimate of initial distance = {0}\nBest Estimate of speed = {1}".format(distEst, speedEst))