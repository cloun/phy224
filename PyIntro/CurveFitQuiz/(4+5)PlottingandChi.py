import matplotlib.pyplot as plt
import numpy as np

def posFunction(d0, u, t):
    return d0 + u*t

def ChiCalc(yValues, predictValues, yUncertainties, numParams):
    """
    Returns the chi squared to characterize the fit of a curve
    
    INPUTS:
    yValues (array): y values of the measured data
    predictValues (array): corresponding y values of the curve fit
    yUncertainties (array): uncertainties associated with the measured y values
    numParams (int): number of paramters in the curve fit
    """

    insideSum = (yValues - predictValues)**2/(yUncertainties**2)

    return (1/(len(yValues) - numParams))*np.sum(insideSum)

# unpacking data
times, pos, uncertainty = np.loadtxt(fname = 'rocket.csv', delimiter = ',', unpack = True, skiprows = 1)

# determining average speed, for the second prediction function
averageSpeed = np.mean(pos[1:]/times[1:])

# determining best estimates of initial pos and speed
averageDist = np.mean(pos)
averageTime = np.mean(times)

speedEst = np.sum((times - averageTime)*(pos - averageDist))/(np.sum((times - averageTime)**2))

distEst = averageDist - (speedEst*averageTime)


# predicting y values using linear regression
linRegPredict = posFunction(distEst, speedEst, times)
# predicting y values using average speed
meanSpeedPredict = posFunction(0, averageSpeed, times)

linRegChi = ChiCalc(pos, linRegPredict, uncertainty, 2)
meanSpeedChi = ChiCalc(pos, meanSpeedPredict, uncertainty, 2)

print('Chi squared for Linear Regression = {0}\nChi squared for average speed = {1}'.format(linRegChi, meanSpeedChi))
print('Linear Regression resulted in a better fit, with a Chi squared of {0:.3f}'.format(linRegChi))

plt.xlabel(r'Time (hours)')
plt.ylabel(r'Position (km)')
plt.title('Position of Saturn V Rocket with uncertainties')
plt.errorbar(times, pos, yerr = uncertainty, ls = 'none', elinewidth = 2., label = 'Measured Position with Uncertainties')
plt.plot(times, posFunction(distEst, speedEst, times), color = 'green', label = 'Predicted Position (linear regression)')
plt.plot(times, posFunction(0, averageSpeed, times), color = 'red', label = 'Predicted Position (mean speed)')
plt.legend(loc  = 'upper left')


plt.savefig('SaturnVPosWithPrediction.pdf')
plt.show()