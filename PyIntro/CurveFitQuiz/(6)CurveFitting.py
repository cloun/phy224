import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

# model function that curve_fit will try to fit the data to
def model_func(t, d0, u):
    return d0+u*t

# calculates Chi squared for a model
def ChiCalc(yValues, predictValues, yUncertainties, numParams):
    """
    Returns the chi squared to characterize the fit of a curve
    
    INPUTS:
    yValues (array): y values of the measured data
    predictValues (array): corresponding y values of the curve fit
    yUncertainties (array): uncertainties associated with the measured y values
    numParams (int): number of paramters in the curve fit
    """

    insideSum = (yValues - predictValues)**2/(yUncertainties**2)

    return (1/(len(yValues) - numParams))*np.sum(insideSum)

# unpacking data
times, pos, uncertainty = np.loadtxt(fname = 'rocket.csv', delimiter = ',', unpack = True, skiprows = 1)

# getting the linear regression values for d0 and u
averageDist = np.mean(pos)
averageTime = np.mean(times)

speedEst = np.sum((times - averageTime)*(pos - averageDist))/(np.sum((times - averageTime)**2))

distEst = averageDist - (speedEst*averageTime)
# getting y values from the linear regression d0 and u value
linRegPredict = model_func(times, distEst, speedEst)

# running curve fit with the linear regression values as the estimates
popt, pcov = curve_fit(model_func, times, pos, sigma = uncertainty, absolute_sigma= True)

pstd = np.sqrt(np.diag(pcov))

# getting predicted y values using the curve_fit parameters
curveFitPredict = model_func(times, popt[0], popt[1])

print("Using curve_fit:\nd0 = {0:.3f} km\nu = {1:.3f} km/h".format(popt[0], popt[1]))
print("Std. Dev. of d0 = {0:.3f} km\nStd. Dev of u = {1:.3f} km/h".format(pstd[0], pstd[1]))
print("Chi squared for the curve_fit = {0:.3f}".format(ChiCalc(pos, curveFitPredict, uncertainty, 2)))

# plotting
plt.xlabel(r'Time (hours)')
plt.ylabel(r'Position (km)')
plt.title('Position of Saturn V Rocket with uncertainties')
plt.errorbar(times, pos, yerr = uncertainty, ls = 'none', elinewidth = 2., label = 'Measured Position with Uncertainties')
plt.plot(times, linRegPredict, color = 'green', label = 'Predicted Position (linear regression)')
plt.plot(times, curveFitPredict, color = 'red', label = 'Predicted Position (curve_fit)')
plt.legend(loc  = 'upper left')

plt.savefig('curve_fitVslinear_regression.pdf')

plt.show()