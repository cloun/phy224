import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# modelling function for free fall of a body
def model_func(t, s0, u, a):
    """
    Function that models the fall of an object from height s0

    INPUTS:
    t (float / array): time at which you want the position of the object
    s0 (float): initial height of the object
    u (float): initial speed of object
    a (float): acceleration due to gravity experienced by the object\
    
    OUTPUTS:
    d (float / array): height of the object
    """
    return s0 + u*t + 0.5*a*(t**2)

# loading data
time, pos, uncertainty = np.loadtxt('feather.csv', delimiter = ',', skiprows= 1, unpack= True)

# curve_fitting
popt, pcov = curve_fit(model_func, time, pos, p0 = [1.75, 0., 1.4], sigma = uncertainty, absolute_sigma= True)
pstd = np.sqrt(np.diag(pcov))

# getting predicted y values with curve_fit params
predictedPos = model_func(time, popt[0], popt[1], popt[2])

# printing out curve_fit parameters
print('Curve Fit Parameters:')
print('Initial Height = {0:3.2f} +/- {1:3.1} m'.format(popt[0], pstd[0]))
print('Initial Velocity = {0:3.2f} +/- {1:3.1} m/s'.format(popt[1], pstd[1]))
print('Acceleration due to Gravity = {0:3.2f} +/- {1:3.1} m/s^2'.format(popt[2], pstd[2]))

plt.ylabel('Height (m)')
plt.xlabel('Time (s)')
plt.title('Position of a falling feather on the moon')
plt.errorbar(time, pos, yerr = uncertainty, linestyle = 'none', elinewidth = 2., label = 'Measured Data')
plt.plot(time, predictedPos, color = 'red', label = 'Curve Fitted Function')
plt.legend(loc = 'upper right')

plt.savefig('FallingFeather.pdf')
plt.show()