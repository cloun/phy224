import numpy as np

def squareSum(a, b):
    return np.sum(np.arange(a, b+1)**2)
