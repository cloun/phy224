import numpy as np

def calcGravity(T, l):
    """

    INPUTS:
    T (float): period of oscillation
    l (float): length of pendulum

    OUTPUTS:
    g (float): acceleration due to gravity
    """
    return (4*np.pi**2*l/(T**2))

print("Gravity = ", calcGravity(5.16, 2.50))

def gravUncertainty(T, l, TUncertainty, lUncertainty):
    """

    INPUTS:
    T (float): period of oscillation
    l (float): length of pendulum
    TUncertainty (float): Uncertainty in period
    lUncertainty (float): Uncertainty in length of pendulum rod

    OUTPUTS:
    g (float): uncertainty in acceleration due to gravity
    """
    TSquareUncertainty = T**2*2*TUncertainty/T
    return (4*np.pi**2*l/T**2)*np.sqrt((TSquareUncertainty/T**2)**2 + (lUncertainty/l)**2)


print("Uncertainty = ", gravUncertainty(5.0, 2.4, 0.01, 0.01))
