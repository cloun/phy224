import numpy as np

def agrees(measurement, uncertainty, actual):
    if(abs(measurement - actual)<=uncertainty):
        return True
    else:
        return False

measurements = [19.2, 19.5, 19.5]
uncertainties = [0.1, 0.8, 0.1]
actual = 19.41


print("Measurement  Uncertainty Agrees")
for i in range(3):
    print("{0}  {1} {2}".format(measurements[i], uncertainties[i], agrees(measurements[i], uncertainties[i], actual)))