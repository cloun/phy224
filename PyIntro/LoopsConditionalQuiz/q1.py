def gpa(grade):
    if grade >= 85:
        return 4.0

    if grade >= 80:
        return 3.7

    if grade >= 77:
        return 3.3
        
    if grade >= 73:
        return 3.0

    if grade >= 70:
        return 2.7

    if grade >= 67:
        return 2.3

    if grade >= 63:
        return 2.0

    if grade >= 60:
        return 1.7
    
    if grade >= 57:
        return 1.3
    
    if grade >= 53:
        return 1.0
    
    if grade >= 50:
        return 0.7
    else:
        return 0