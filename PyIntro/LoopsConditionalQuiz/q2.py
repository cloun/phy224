import numpy as np

def gpa(grade):
    if grade >= 85:
        return 4.0

    if grade >= 80:
        return 3.7

    if grade >= 77:
        return 3.3
        
    if grade >= 73:
        return 3.0

    if grade >= 70:
        return 2.7

    if grade >= 67:
        return 2.3

    if grade >= 63:
        return 2.0

    if grade >= 60:
        return 1.7
    
    if grade >= 57:
        return 1.3
    
    if grade >= 53:
        return 1.0
    
    if grade >= 50:
        return 0.7
    else:
        return 0

marks= np.array([72, 82, 72, 72, 79, 57, 59, 71, 66, 80,
   67, 62, 91, 74, 77, 62, 71, 78, 65, 80,
   70, 74, 70, 95, 76, 66, 85, 64, 79, 57,
   63, 78, 84, 78, 75, 73, 62, 69, 72, 87])

totalGPA = 0

for value in marks:
   totalGPA += gpa(value)

print("Average GPA is ", totalGPA/len(marks))