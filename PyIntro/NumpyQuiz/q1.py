import numpy as np
a=1
b = np.array([3.0,2.3,1.0])
c = np.array([3,.3,.03])
d = np.array([[2,4],[4,6],[7,8]])
f = [9,90,900]

print('a+b:', a+b)

print('c+b:', c+b)

g = np.array([])
# there must be a better way to do this but idk how
for i in np.arange(len(c)):
    g=np.append(g, d[i]+c[i])
g=np.reshape(g, (3,2))
print('c+d:', g)

print('b+f:', b+f)

print('type d:', type(d[0]),'type f:', type(f[0]))

print('length of d:', len(d), 'length of f:', len(f))