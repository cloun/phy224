import numpy as np

t= np.array([0. , 0.1155, 0.2287, 0.3404, 0.4475, 0.5546, 0.6607, 0.7753, 0.8871, 1. ])
y= np.array([ 0. , 0.1655, 0.2009, 0.1124, -0.0873, -0.3996, -0.8197, -1.3977, -2.0856, -2.905 ])

print('Average of y:', np.average(y))
print('Sample std dev. of y', np.std(y, ddof=1))

dy = np.diff(y)
dt = np.diff(t)
print('differential of y:',dy/dt)

E = 0.5*y**2
# taking the left riemann sum (ignoring the last energy element)
print('integral of energy:', np.sum(E[:-1]*dt))