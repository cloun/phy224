import matplotlib.pyplot as plt
from scipy import stats

values = stats.norm.rvs(loc = 3, scale = 1, size = 1000)

plt.hist(values, density=True, bins = 25)
plt.xlabel('Value')
plt.ylabel('Percentage of Occurences')

plt.show()