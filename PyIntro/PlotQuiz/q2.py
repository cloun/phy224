import matplotlib.pyplot as plt
import numpy as np

# defines the function
def func(t, tau):
    return (np.exp(-(t/tau)))

t = np.arange(0, 100, 0.1)

# first y values with tau = 11
y0 = func(t, 11)
# second with tau = 10
y1 = func(t, 10)

# plotting with regular y scale
plt.subplot(2, 1, 1)
plt.plot(t, y0, c = 'r', label = r'$\tau$ = 11')
plt.plot(t, y1, c= 'b', label = r'$\tau$ = 10')
plt.legend(loc = 'upper right')
plt.ylabel('y')

# plotting with log y scale
plt.subplot(2, 1, 2)
plt.yscale('log')
plt.plot(t, y0, c = 'r', label = r'$\tau$ = 11')
plt.plot(t, y1, c= 'b', label = r'$\tau$ = 10')
plt.legend(loc = 'upper right')
plt.ylabel('y')
plt.xlabel('Time (s)')


plt.show()