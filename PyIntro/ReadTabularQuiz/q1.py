import numpy as np

times = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
positions = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55]

# transposes the arrays so they're saved as columns instead of rows in the file
np.savetxt('data.txt', np.transpose([times, positions]), fmt='%.2f', header= 'Time    Position')