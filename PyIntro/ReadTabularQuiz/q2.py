import numpy as np

# array of month names for easy conversion since the months are stored as numbers in the csv file
months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

# there were 52 rows before the data 
year, month, averageCO2 = np.loadtxt('co2_mm_mlo.csv', delimiter=',', skiprows = 52, usecols= [0, 1, 4], unpack= True)

# prints out the average CO2 output, formatted to look a bit better
for i in range(len(year)):
    print('In {0} '.format(int(year[i])), months[int(month[i])-1], ', Average CO2 output was {0} tons'.format(averageCO2[i]))