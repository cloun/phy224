from scipy import stats
import numpy as np

# setting the variables for generating the distribution
center = 3.
stdev = 1.
numValues = 1000

# generates the distribution
values = stats.norm.rvs(loc = center, scale = stdev, size= numValues)

# prints out the mean, variance, and stdev of the dist.
print('mean: ', stats.norm.mean(loc = center, scale = stdev), '\nvariance: ', stats.norm.var(loc = center, scale = stdev), '\nst dev.: ', stats.norm.std(loc = center, scale = stdev))

# percentages of numbers that fall within a range of st.devs
num1Dev = 0
num2Dev = 0
num3Dev = 0

for value in values:
    if(center- 3*stdev <= value <= center + 3*stdev):
        num3Dev += 1

        if(center - 2*stdev <= value <= center + 2*stdev):
            num2Dev += 1

            if(center -stdev <= value <=center + stdev):
                num1Dev +=1

print("Percentage of Values within one std. dev.: ", num1Dev/numValues*100, '%')
print("Percentage of Values within two std. dev.: ", num2Dev/numValues*100, '%')
print("Percentage of Values within three std. dev.: ", num3Dev/numValues*100, '%')