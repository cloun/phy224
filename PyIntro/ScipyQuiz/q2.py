from scipy import optimize as opt
import numpy as np

# defining the function
def func(x):
    return x**9 - 3*x**6 + x**2 + 1

# the function diverges to +/- infinity as x goes to +/- infinity, so if we pick a sufficiently large interval all the roots will be found
# It seems that no matter how much larger the interval gets, no more roots are found, so I left the interval at -100 to 100
start = -100.
stop = 100.
step = 0.1

x = np.arange(start, stop, step)
y = func(x)

# starting indices
i0 = 0
i1 = 1

roots = []
# basically looks at every index and checks if the sign of the function changes from x = x[i0] to x = x[i1]
# if the sign changes then x[i0] and x[i1] are inputted into the brentq algorithm and the root is appended to the list of roots
for i in np.arange(1, len(x)):
    i1 = i
    if(np.sign(y[i0]) != np.sign(y[i1])):
        
        roots.append(opt.brentq(func, x[i0], x[i1]))
        i0 = i

print(roots)