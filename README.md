Stuff for PHY224 2nd Year Lab Course
Quick Descriptions below, see the lab reports in each folder for more information
Starred projects took the most work


IntroLab and PyIntro: Introductory material, nothing interesting.

OhmsLaw: Verified Ohm's Law and the Power Law for black bodies by plotting the current in a circuit against the voltage

Polarization of Light: Verified Malus' Law and Determining Brewster's Angle by fitting plots of light intensity against a varying angle of incidence with various setups.

Power Supply Lab: Determined open circuit voltage and internal resistance of a battery and a power supply

Radioactive Decay: Determined the half-life of a Ba-137 by fitting the emissions to a half life equation after considering background. Also compared the emissions of a Fiesta plate (with a uranium oxide glaze) to a Poisson and Gaussian distribution.

Spring Oscillator Lab: Compared measured motion of a damped and undamped spring oscillator to a simulation using various numerical integration methods.

Radioactivity in Air*: Made an estimate of the concentration of Radon in the atmosphere. Required lots of data cleaning due to background radiation, and emissions from other isotopes that could not be accounted for in the curve fit.

Electron Spin Resonance*: Determined the gyromagnetic ratio for an electron in a sample of DPPH (diphenylpicryl hydrazyl), and the corresponding Lande g factor. Required lots of oscilloscope work.