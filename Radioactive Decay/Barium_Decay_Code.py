import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

# linear model function
def lin_model_func(t, halfLife, int0):
    return np.log(int0) + (t/halfLife)*np.log(0.5)

# non-linear model function
def model_func(t, halfLife, int0):
    return int0*(0.5**(t/halfLife))

def chiValue(yValues, yErrors, predictedValues, numParams):
    insideSum = (yValues - predictedValues)/yErrors
    insideSum = insideSum**2

    return 1/(len(yValues)-numParams)*np.sum(insideSum)

samples, backgroundData = np.loadtxt('Barium_Background.txt', unpack= True, skiprows = 2)
totalData = np.loadtxt('Barium_Decay.txt', unpack=True, skiprows= 2, usecols= 1)
# determining mean backgroud rad
meanBackground = np.mean(backgroundData)
# estimating counts due to barium alone and std.
bariumData = totalData - meanBackground
bariumDataStd = np.sqrt(totalData + meanBackground)
# converting into rates (assume each datapoint is taken over 20 seconds)
bariumRates = bariumData/20
bariumRatesStd = np.sqrt(bariumData)/20
# log values
logBariumRates = np.log(bariumRates)
logBariumRatesStd = np.abs(bariumRatesStd/bariumRates)
# x data
times = samples*20

# performing linear regression
poptLin, pcovLin = curve_fit(lin_model_func, times, logBariumRates, p0 = [2.6*60, 1.], sigma = logBariumRatesStd, absolute_sigma= True)

# CALCULATIONS FOR LINEAR
# y values
yLin = model_func(times, poptLin[0], poptLin[1])
# std. dev
stdLin = np.sqrt(np.diag(pcovLin))
# chi squared
chiLin = chiValue(bariumRates, bariumRatesStd, model_func(times, poptLin[0], poptLin[1]), 2)


# performing non-linear curve fit
popt, pcov = curve_fit(model_func, times, bariumRates, p0 = [2.6*60, -1.], sigma = bariumRatesStd, absolute_sigma= True)

# CALCULATIONS FOR NONLINEAR
# y values
y = model_func(times, popt[0], popt[1])
# std dev
std = np.sqrt(np.diag(pcov))
# chi squared
chi = chiValue(bariumRates, bariumRatesStd, model_func(times, popt[0], popt[1]), 2)


# printing out relevant values
print("Linear Curve Fit:")
print("Half Life: {0:.3f} +/- {1:.1} s".format(poptLin[0], stdLin[0]))
print("Coefficient: {0:.3f} +/- {1:.1}".format(poptLin[1], stdLin[1]))
print("Reduced Chi squared: {0:.2f}".format(chiLin))
print()
print("Non-Linear Curve Fit:")
print("Half Life: {0:.3f} +/- {1:.1} s".format(popt[0], std[0]))
print("Coefficient: {0:.3f} +/- {1:.1}".format(popt[1], std[1]))
print("Reduced Chi squared: {0:.2f}".format(chi))
print()
print("Expected half life: {0:.3f} s".format(2.6*60))


# plotting
plt.figure(figsize= (10, 6))
# comment out the respective section and uncomment the other to switch between log and regular graphs
# LOG STUFF
#plt.yscale('log')
#plt.title('Rate of Emission over time for a Ba-137m sample (emissions logged)')
#plt.ylabel('Logged emissions per second')
#plt.errorbar(times, bariumRates, np.exp(logBariumRatesStd), ls = 'none', label = 'Measured Data')
# REGULAR STUFF
plt.errorbar(times, bariumRates, bariumDataStd, ls = 'none', label = "Measured Data")
plt.title('Rate of Emission over time for a Ba-137m sample')
plt.ylabel('Emissions per Second')

plt.xlabel('Time (s)')
plt.plot(times, model_func(times, poptLin[0], poptLin[1]), label = 'Linear Regression')
plt.plot(times, model_func(times, popt[0], popt[1]), label = 'Non-Linear Curve-Fit')
plt.legend(loc = 'upper right')

plt.show()