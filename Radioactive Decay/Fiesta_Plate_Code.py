import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from scipy.stats import poisson
from scipy.stats import norm
from scipy.stats import tstd

# loading data
samples, background = np.loadtxt('fiesta_background.txt', skiprows = 2, unpack = True)
total = np.loadtxt('fiesta_data.txt', skiprows = 2, unpack=True, usecols= 1)

avgBackground = np.mean(background)
print('Average Background Radiation: {0:.3f}'.format(avgBackground))
print('Std. Dev for Gaussian (Background): {0:.3f}'.format(np.sqrt(avgBackground)))
# since the background radiation is measured over 20 seconds per sample, but the fiesta plate was 3 seconds per sample
# I multiplied the avg background by 3/20 so it would be ~the num of background ticks per 3 second interval
fiesta = total - avgBackground*(3/20)
# std dev
fiestaStd = np.sqrt(total + avgBackground*(3/20))

# average ticks per sample
avgTicks = np.mean(fiesta)
print('Average emissions per sample period: {0:.3f}'.format(avgTicks))
print('Std. Dev for Gaussian (Fiesta): {0:.3f}'.format(np.sqrt(avgTicks)))

# x values for fiesta
xPoisson = np.arange(poisson.ppf(0.0001, avgTicks), poisson.ppf(0.9999, avgTicks))
xGaussian = np.arange(norm.ppf(0.0001, avgTicks, np.sqrt(avgTicks)), norm.ppf(0.9999, avgTicks, np.sqrt(avgTicks)))
# x values for background
xPoissonBg  = np.arange(poisson.ppf(0.0001, avgBackground), poisson.ppf(0.999, avgBackground))
xGaussianBg = np.arange(norm.ppf(0.0001, avgBackground, np.sqrt(avgBackground)), norm.ppf(0.9999, avgBackground, np.sqrt(avgBackground)))


plt.figure(figsize=(10, 8))
plt.subplots_adjust(left = 0.125, bottom = 0.1, right = 0.9, top = 0.9, wspace = 0.2, hspace = 0.3)
# fiesta plate radiation histogram
plt.subplot(2, 1, 1)
plt.ylabel('Number of Occurences')
plt.xlabel('Emissions per Sample Period')
plt.title('Alpha Particles Emitted by a Fiesta Plate')
plt.hist(fiesta, bins = 90, label = 'Measured Data')
plt.plot(xPoisson, len(samples)*poisson.pmf(xPoisson, avgTicks), label = 'Poisson Mass Function')
plt.plot(xGaussian, len(samples)*norm.pdf(xGaussian, avgTicks, np.sqrt(avgTicks)), label = 'Gaussian Distribution')
plt.legend(loc= 'upper left')
# backround radiation histogram
plt.subplot(2, 1, 2)
plt.ylabel('Number of Occurences')
plt.xlabel('Emissions per Sample Period')
plt.title('Background Radiation')
plt.hist(background, bins = 90, label = 'Background Radiation Measurements')
plt.plot(xPoissonBg, len(samples)*poisson.pmf(xPoissonBg, avgBackground), label = 'Poisson Mass Function')
plt.plot(xGaussianBg, len(samples)*norm.pdf(xGaussianBg, avgBackground, np.sqrt(avgBackground)), label = 'Gaussian Distribution')
plt.legend(loc= 'upper left')

plt.show()