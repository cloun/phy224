import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# returns uncertainty carrying over through addition
def addUncertainty(uncertainties):
    uncertainties = np.array(uncertainties)
    return np.sqrt(np.sum(uncertainties**2))

# returns the uncertainty carrying through multiplication
def multiplyUncertainty(values, uncertainties, finalValue):
    values = np.array(values)
    uncertainties = np.array(uncertainties)

    rootValue = np.sqrt(np.sum((uncertainties/values)**2))
    return abs(finalValue)*rootValue

# returns chi squared to determine goodness of fit
def chiValue(yValues, yErrors, predictedValues, numParams):
    insideSum = (yValues - predictedValues)/yErrors
    insideSum = insideSum**2

    return 1/(len(yValues)-numParams)*np.sum(insideSum)

# BACKGROUND RADIATION WORK
# extracts the counts per sample period (20 seconds)
bgCounts = np.loadtxt('Data/BackgroundRad2Hrs20SecSample.txt', unpack = True, usecols = 1, skiprows = 2)
bgCounts2 = np.loadtxt('Data/BackgroundRad2Hrs20SecSample2.txt', unpack = True, usecols = 1, skiprows = 2)

# determines average background radiation (with uncertainty)
bgCountUnc = np.sqrt(bgCounts)
avgBgUnc = addUncertainty(bgCountUnc)/len(bgCountUnc)
avgBg = np.mean(bgCounts)
print('Average Background Radiation (counts/sec) = {0:.3f} +/- {1:.1}'.format(avgBg/20, avgBgUnc/20))

bgCountUnc2 = np.sqrt(bgCounts2)
avgBgUnc2 = addUncertainty(bgCountUnc2)/len(bgCountUnc2)
avgBg2 = np.mean(bgCounts2)
print('Average Background Radiation in counts/sec = {0:.3f} +/- {1:.2}'.format(avgBg2/20, avgBgUnc2/20))

# DUST RADIATION WORK
sampleNum, dustCounts = np.loadtxt('Data/AirDustRad20SecSample.txt', unpack = True, skiprows = 2)
# getting the dust count rate and uncertainty in rate
dustRate = dustCounts/20
dustUnc = np.sqrt(dustCounts)/20

# I set the time values to be the midpoints of each sample period, so the rate in counts/sec would be at slightly more accurate times
times = sampleNum * 20 - 10

# subtracting background and getting the new uncertainty
dustRateIsolated = dustRate - (avgBg/20)
for i in range(len(dustUnc)):
    dustUnc[i] = addUncertainty([dustUnc[i], avgBgUnc/20])

# makes sure all the rate values are positive
for i in range(len(dustRateIsolated)):
    if dustRateIsolated[i] <= 0:
        dustRateIsolated[i] = 0.5

# constants:
# decay rate for lead-214 (half life in seconds)
gammaPb = np.log(2)/(27*60)
# decay rate for Bi-214 (half life in seconds)
gammaBi = np.log(2)/(19.9*60)
# decay rate for Pb-212 (half life in seconds)
gammaPb212 = np.log(2)/(10.64*60*60)
# ratio of epsilons
epsR = 1.19


# model function for the curve_fit for the dust radiation
def model_func(time, R, count0):
    # just splitting up the equation into multiple parts to make it easier
    pt1 = count0 * 0.8 * (1 + epsR*( gammaBi/(gammaBi - gammaPb) ) ) * np.exp(-gammaPb*time)

    pt2 = count0 * 0.8 * epsR * (R - gammaBi/(gammaBi - gammaPb) ) * np.exp(-gammaBi * time)

    return pt1 + pt2


def Pb212Activity(time, count0):
    return count0 * np.exp(-gammaPb212*time)

def ln_Pb212Activity(time, count0):
    return np.log(count0) - gammaPb212*time

# index at which Pb212 activity takes over
pb212Index = 400

poptPb212, pcovPb212 = curve_fit(Pb212Activity, times[pb212Index:], dustRateIsolated[pb212Index:], p0 = [0.20], sigma = dustUnc[pb212Index:], absolute_sigma = True)
pb212Unc = np.sqrt(pcovPb212)[0]
pb212Rates = Pb212Activity(times, 0.3)

pb212ChiSquare = chiValue(dustRateIsolated[pb212Index:], dustUnc[pb212Index:], pb212Rates[pb212Index:], 1)

dustRateIsolated = dustRateIsolated - pb212Rates

# curve fit for Pb214 and Bi214
poptDust, pcovDust = curve_fit(model_func, times, dustRateIsolated, p0 = [1.41, 3.], sigma = dustUnc, absolute_sigma= True)
poptDustUnc = np.sqrt(np.diag(pcovDust))

# getting the y values for the curve fit
yValuesDust = model_func(times, poptDust[0], poptDust[1])
# getting the chi squared reduced
chiSquare = chiValue(dustRateIsolated, dustUnc, yValuesDust, 2)


print('Curve Fit Values:')
print('R: {0:.2f} +/- {1:.2}'.format(poptDust[0], poptDustUnc[0]))
print('R Observed: {0:.2f} +/- {1:.2}'.format(poptDust[0]*1.19, poptDustUnc[0]*1.19))
print('Initial Pb Activity: {0:.2f} +/- {1:.1} counts/sec'.format(poptDust[1], poptDustUnc[1]))
print('Chi Square Reduced: {0:.3}'.format(chiSquare))
print("Pb212 Activity: {0:.1f} +/- {1:.1} counts/sec".format(poptPb212[0], np.sqrt(pcovPb212[0])[0] ))
print('Chi Square Reduced for Pb212 Fit: {0:.3}'.format(pb212ChiSquare))

# determines the residuals
residualY = dustRateIsolated - yValuesDust

# determining the concentration of Radon in the air and its uncertainty
# concentration in atoms per cubic meter of air
N = (poptDust[1]/0.3)/(60*0.8)
# uncertainty
NUnc = multiplyUncertainty([poptDust[1]/0.3, 60, 0.8], [poptDustUnc[1]/0.3, 1., 0.1], N)

print('Concentration of Rn in Atmosphere: {0:.2} +/- {1:.1} Bq/m^3'.format(N, NUnc))

# comparing the second background radiation 
plt.figure(figsize = (8,5))
plt.title('Background Radiation Taken over a 2 Hour Period', fontsize = 13)
plt.ylabel('Number of Occurences', fontsize = 12)
plt.xlabel('Counts/Second', fontsize = 12)
plt.hist(bgCounts/20, bins =20)

plt.figure(figsize = (8,5))
plt.title('Background Radiation Taken over a 1 Hour Period', fontsize = 13)
plt.ylabel('Number of Occurences', fontsize = 12)
plt.xlabel('Counts/Second', fontsize = 12)
plt.hist(bgCounts2/20, bins = 15)

# plotting the dust activity and residuals
plt.figure(figsize = (10, 8))
plt.subplot(2, 1, 1)
plt.xlabel('Time (s)', fontsize = 12)
plt.ylabel('Rate (counts/second)', fontsize = 12)
plt.title('Radiation of an air sample taken over 1 hour', fontsize = 13)
plt.errorbar(times, dustRateIsolated, yerr = dustUnc, ls = 'none', label = 'Adjusted Measured Data')
plt.plot(times, yValuesDust, label = 'Curve Fit to Equation 5', color=  'red')
plt.legend(loc = 'upper right')

plt.subplot(2, 1, 2)
plt.xlabel('Time (s)', fontsize = 12)
plt.ylabel('Rate (counts/second)', fontsize = 12)
plt.title('Residuals (Measured - Predicted rates)', fontsize = 13)
plt.errorbar(times, residualY, yerr =dustUnc, label = 'Residuals')

# # log plotting
# plt.semilogy(times, dustRateIsolated)
# plt.semilogy(times, pb212Rates)


plt.tight_layout()
plt.show()