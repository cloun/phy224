import numpy as np
import matplotlib.pyplot as plt

# returns the uncertainty due to addition
def addUncertainty(uncertainties):
    uncertainties = np.array(uncertainties)
    return np.sqrt(np.sum(uncertainties**2))

# array stores sets of counts per sample, based on the voltage (stored in another array at the same index)
voltage = []
samples = []
# stores uncertainties associated with each sample
sampleUnc = []

# appends each set of data to the samples array, along with the associated voltage to the voltage array
for i in np.arange(700, 1550, 50):
    storeSamples = np.loadtxt(fname = 'Data/CS137_Radiation_Data/'+str(i)+'V3min20secSamplesCS137.txt', skiprows = 2, usecols = 1, unpack = True)
    samples.append(storeSamples)
    sampleUnc.append(np.sqrt(np.array(storeSamples)))

    voltage.append(i)

# stores the average counts per voltage
avgCounts = []
# stores the uncertainties related to the averages
avgCountsUnc = []

# determines averages and uncertainties
for sampleSet in samples:
    avgCounts.append(np.mean(sampleSet))
    avgCountsUnc.append(addUncertainty(sampleUnc)/len(sampleSet))

# plots the average counts against the voltages, with their uncertainties
plt.figure(figsize = (10,5))
plt.title('Average Counts of a Cs-137 Sample with a varying Voltage supply to the Geiger-Mueller Tube', fontsize = 14)
plt.xlabel('Voltage (V)', fontsize = 13)
plt.ylabel('Average Counts per Sample Period (20s)', fontsize = 13)
plt.errorbar(voltage, avgCounts, yerr = avgCountsUnc, ls = 'none', color = 'blue')
plt.scatter(voltage, avgCounts, color = 'blue')

plt.show()