import numpy as np
import matplotlib.pyplot as plt

timeMeasured, posMeasured = np.loadtxt('posDataDampedLonger.txt', unpack = True, skiprows = 2, usecols = (0, 1))
mass = 0.2012

# determining the max amplitude given the min and max pos near the beginning of the measurement
minPos = 18.441
maxPos = 20.938
equil = (maxPos + minPos)/2
maxAmp = (maxPos - minPos)/2
print('Max Amplitude: {0}\n(1/e)*Max Amp: {1:.3f}'.format(maxAmp, maxAmp/(np.e)))

# time at which the amplitude is (1/e)*max amplitude
timeDamp = 164.39
# calculating gamma
gamma = 2/timeDamp
print('Gamma = {0:.3}'.format(gamma))

# constants for integration
dt = 0.001
v0 = 0
y0 = maxAmp
omega = 8.85
k = omega**2*mass
times = np.arange(0, 200+dt, dt)


y = np.zeros(len(times))
y[0] = y0
v = np.zeros(len(times))
v[0] = v0

# doing the numerical integration
for i in range(len(y)-1):
    y[i+1] = y[i] + v[i]*dt
    v[i+1] = v[i] - omega**2*y[i+1]*dt - gamma*v[i]*dt

# determining energy
E = 0.5*mass*(v*0.01)**2 + 0.5*k*(y*0.01)**2

plt.figure(figsize=(12, 5))
plt.title('Measured Data')
plt.xlabel('Time (s)')
plt.ylabel('Position (cm)')
plt.plot(timeMeasured, posMeasured)

plt.figure(figsize=(14, 8))
plt.subplot(2, 1, 1)
plt.title('Numerical Integration')
plt.xlabel('Time (s)')
plt.ylabel('Position (cm)')
plt.plot(times, y)

plt.subplot(2, 1, 2)
plt.xlabel('Time (s)')
plt.ylabel('Energy (J)')
plt.plot(times, E)

plt.tight_layout()
plt.show()