import numpy as np
import matplotlib.pyplot as plt

timeMeasured, posMeasured = np.loadtxt('posDataUndamped.txt', unpack = True, skiprows = 2, usecols = (0,1)) 

# determining period and amplitude
minPos = 0
maxPos = 0
initTime = 0
finalTime = 0
for i in np.arange(1, len(posMeasured)-1):
    if(posMeasured[i-1]> posMeasured[i] and posMeasured[i+1]> posMeasured[i] and initTime == 0):
        initTime = timeMeasured[i]
        minPos = posMeasured[i]
    elif(posMeasured[i-1]> posMeasured[i] and posMeasured[i+1]> posMeasured[i] and initTime != 0):
        finalTime = timeMeasured[i]
        break

    if(posMeasured[i-1] < posMeasured[i] and posMeasured[i+1] < posMeasured[i] and maxPos ==0):
        maxPos = posMeasured[i]

period = finalTime - initTime
amplitude = (maxPos - minPos)/2

# constants for the numerical integration
mass = 0.2012
dt = 0.001
omega = (2*np.pi)/period
k = omega**2*mass
times = np.arange(0, 10+dt, dt)
v0 = 0
y0 = amplitude

# printing out some values
print('Period: {0:.2} s'.format(period))
print('Amplitude: {0:2} +/- 0.2 cm')
print('Amplitude of Oscillation: {0:.2}'.format(amplitude))
print('Omega: {0:.2f}'.format(omega))
print('Spring Constant: {0:.3} N/m'.format(k))

# values for forward Euler integration
y = np.zeros(len(times))
v = np.zeros(len(times))
v[0] = v0
y[0] = y0

# values for Euler-Cromer method
yECromer = np.zeros(len(times))
vECromer = np.zeros(len(times))
yECromer[0] = y0
vECromer[0] = v0


for i in range(len(times)-1):
    # Forward Euler Integration
    v[i+1] = v[i] - dt*(omega**2)*y[i]
    y[i+1] = y[i] + dt*v[i]

    # Euler-Cromer integration
    yECromer[i+1] = yECromer[i] + vECromer[i]*dt
    vECromer[i+1] = vECromer[i] - (omega**2)*yECromer[i+1]*dt

# determining energy (for both methods)
E = 0.5*mass*(v*0.01)**2 + 0.5*k*(y*0.01)**2
EECromer = 0.5*mass*(vECromer*0.01)**2 + 0.5*k*(yECromer*0.01)**2


# plot for measured values
plt.figure(figsize= (8,4))
plt.plot(timeMeasured, posMeasured)
plt.xlabel('Time (s)')
plt.ylabel('Position (cm)')
plt.title('Measured Data')


# plots for numerical integration
plt.figure(figsize = (14, 8))
plt.subplot(4, 1, 1)
plt.title('Numerical Integration')
plt.xlabel('Time (s)')
plt.ylabel('Velocity (cm/s)')
plt.plot(times, v, label = 'Forward Euler Integration')
plt.plot(times, vECromer, label = 'Euler-Cromer Integration')
plt.legend(loc = 'upper left')

plt.subplot(4, 1, 2)
plt.xlabel('Time (s)')
plt.ylabel('Position (cm)')
plt.plot(times, y, label = 'Forward Euler Integration')
plt.plot(times, yECromer, label = 'Euler-Cromer Integration')
plt.legend(loc = 'upper left')

plt.subplot(4, 1, 3)
plt.xlabel('Position (cm)')
plt.ylabel('Velocity (cm/s)')
plt.plot(y, v, label = 'Forward Euler Integration')
plt.plot(yECromer, vECromer, label = 'Euler-Cromer Integration')
plt.legend(loc = 'upper left')

plt.subplot(4, 1, 4)
plt.xlabel('Time(s)')
plt.ylabel('Kinetic Energy (J)')
plt.plot(times, E, label = 'Forward Euler Integrtion')
plt.plot(times, EECromer, label = 'Euler-Cromer Integration')
plt.legend(loc = 'upper left')

plt.tight_layout()
plt.show()