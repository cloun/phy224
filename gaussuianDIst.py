import numpy as np

measurements = np.array([12.43, 12.28, 12.25, 12.38, 12.12])
average = np.average(measurements)
deviation = 0
for value in measurements:
    deviation+= (value - average)**2

deviation = deviation/(len(measurements)-1)
deviation = np.sqrt(deviation)
print("Average: {0} \n Deviation: {1}".format(average, deviation))