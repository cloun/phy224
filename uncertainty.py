import numpy as np

def powerUncertainty(value, uncertainty, power):
    return (value**power)*power*(uncertainty/value)

def multiplyUncertainty(values, uncertainties, finalValue):
    values = np.array(values)
    uncertainties = np.array(uncertainties)

    rootValue = np.sqrt(np.sum((uncertainties/values)**2))
    return abs(finalValue)*rootValue

def addUncertainty(uncertainties):
    uncertainties = np.array(uncertainties)
    return np.sqrt(np.sum(uncertainties**2))

def IntroLab(period, periodUncertainty, radius, radiusUncertainty,  g):
    period2Uncertainty = powerUncertainty(period, periodUncertainty, 2)
    gravityUncertainty = multiplyUncertainty([radius, period**2], [radiusUncertainty, period2Uncertainty], g)

    return gravityUncertainty


print(addUncertainty([0.239]))
#print(powerUncertainty(0.491, 0.056, 2))
#print(multiplyUncertainty([1.866, 0.23375], [0.055, 0.00025], 9.891))